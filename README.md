# CanvasGameAPI

**Add API**

git remote add -f [name of your remote API] https://gitlab.com/canvasgames/canvasgameapi.git

---------------------------------------------------------------------------------------------

**DOJO**

---------------------------------------------------------------------------------------------

**Tests**

1- Snake

1.1 - Init Snake
* Verify if tiles of snake is the same size of tiles screen
* Verify if snake head is on the tile of the middle of screen

1.2 - Inputs 
* Verify if pressed W the snake go to up and current side is not down
* Verify if pressed D the snake go to right and current side is not left
* Verify if pressed S the snake go to down and current side is not up
* Verify if pressed A the snake go to left and current side is not right

1.3 - Manager 
* Verify if snake is inside the screen
* Verify if snake eat a target
* Verify if snake eat your body

2- Fruit

2.1 - Init Fruit
* Verify if tiles of snake is the same size of tiles screen
* Verify if fruit appears inside the screen

-------------------------------------------------------------------------------------

**Development**

Snake Parts
* Set your width and heigth, that been the width and heigth of blocks of grid game 
* Set a moviment side
* Make the moviment function

Snake (Snake Manager)
* Set the delay of updating of snake
* Create 3 Initial Parts (on Awake)
* Function to verify a button pressed for the head change your side
obs: the snake cant comeback your movimment, example: if go to left, the snake cant go to right
* Function that do all parts follow the last position of the previous SnakePart
* Function that add a new part after the last SnakePart
obs: need to pay attencion because this new part need to be in the empty block

Fruit
* Need to be the same width and height of SnakeParts

GameManager
* Create game config parameter, example size of blocks of gird game
* How many blocks exists in horizontal and vertical in your canvas
* Create the Snake at determinated position
* Function to create the the Fruit and raffle your position inside the canvas screen
(Function that use Collision2D)
* Function to verify if Snake hit with fruit
* Function to verify if Snake head hit with snake parts
* Function to verify if Snake hit with the wall (the limits of canvas)
* Function of Game Over, simple alert with your score and reason of your death
