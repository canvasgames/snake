class GameManager extends GameObject {

    Awake(params){

        //Game Config
        this.gridBlockSize = 50
        this.gameDelay = 500

        //Auto Config Game
        this.horizontalBlocks = canvas.width / this.gridBlockSize
        this.verticalBlocks = canvas.height / this.gridBlockSize
        this.score = 0

        //Initialize Objects
        this.minimunSizeToCollide = 4
        this.snake = new Snake(0, 0, {size: this.gridBlockSize, delay: this.gameDelay})
        this.CreateFruit()


    }

    Update() {

        this.SnakeHitTarget()
        this.SnakeHitSnake()
        this.SnakeHitWall()

    }

    SnakeHitTarget() {
        if(Collision2D.Rect(this.snake.GetHead(), this.fruit)) {
            this.snake.AddPartToEnd()
            this.fruit.Destroy()
            this.score += 1
            this.CreateFruit()
        }
    }

    SnakeHitSnake() {
        if(this.snake.myParts.length > this.minimunSizeToCollide){
            for(let i = this.minimunSizeToCollide - 1; i <= this.snake.myParts.length - 1; i++){
                let currentPart = this.snake.myParts[i]
                if(Collision2D.Rect(this.snake.GetHead(), currentPart)) {
                    this.GameOver("Eat my " + (i+1) + " part...")
                }
            }
        }
    }

    SnakeHitWall() {
        if(!Collision2D.Rect(this.snake.GetHead(), {x: 0, y: 0, w: canvas.width, h: canvas.height})) {
            this.GameOver("Hit the wall...")
        }
    }

    CreateFruit() {
        const fPos = this.RaffleFruitPosition()
        this.fruit = new Fruit(fPos.x, fPos.y, {size: this.gridBlockSize, id: this.score + 1})
    }

    RaffleFruitPosition() {

        return {
            x: parseInt(Math.random() * this.horizontalBlocks) * this.gridBlockSize,
            y: parseInt(Math.random() * this.verticalBlocks) * this.gridBlockSize,
        }

    }

    GameOver(msg) {
        alert(msg + " Your Final Score: " + this.score)
        location.reload()
    }

    Draw (ctx) {

    }

}