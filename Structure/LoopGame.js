//TODO: make this class work
class LoopGame {

    constructor(canvas, context) {



        setInterval(
            () => {
                
                context.clearRect(0, 0, canvas.width, canvas.height)
                GameObjectManager.AllGameObjects.forEach(gameobject => {
                    if(gameobject){
        
                        gameobject.Update()
        
                        context.save()
        
                        context.beginPath()
                        gameobject.Draw(context)
                        context.closePath()
                        
                        context.restore()
        
                    }
                });
        
            },
            Frame.CurrentFrameRate()
        )

    }

}

