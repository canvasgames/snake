let GameObjectManager = {

    AllGameObjects: [],
    AddGameObject: (gameObject) => {
        const self = GameObjectManager
        self.AllGameObjects[gameObject.hashCode] = gameObject
        if(gameObject.tag) self.AddTaggegGameObject(gameObject)
        self.GenerateHasCode()
    },

    TaggedGameObjects: [],
    AddTaggegGameObject: (gameObject) => {
        const self = GameObjectManager
        if(!self.TaggedGameObjects[gameObject.tag]){
            self.TaggedGameObjects[gameObject.tag] = []
        }
        self.TaggedGameObjects[gameObject.tag][gameObject.hashCode] = gameObject
    },

    DestroyGameObject: (gameObject) => {
        const self = GameObjectManager
        delete self.AllGameObjects[gameObject.hashCode]
        if(self.TaggedGameObjects[gameObject.tag]){
            delete self.TaggedGameObjects[gameObject.tag][gameObject.hashCode]
        }
    },
    
    HashCode: "1",
    GenerateHasCode: () => {
        const self = GameObjectManager
        self.HashCode = (parseInt(self.HashCode) + 1).toString()
    },
}